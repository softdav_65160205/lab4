/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player player1;
    private Player player2;
    private Player currentplayer;
    private int turncount = 0;
    private int row, col;


    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentplayer = player1;
    }


    public char[][] gettable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentplayer;
    }


    public  boolean setRowCol(int row, int col) {
        if (table[row-1][col-1] != '-') {
            return false;
        }
        table[row -1][col -1] = currentplayer.getSymbol();
        this.row = row;
        this.col = col;
        return true;
    }


    public boolean checkWin() {
        if (checkRow() || checkCol() || checkX()) {
            return true;
        }
        return false;
    }

    private boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentplayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentplayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkX() {
        if (table[0][0] != '-' && table[0][0] == table[1][1] && table[2][2] == table[1][1] || table[0][2] != '-' && table[0][2] == table[1][1] && table[2][0] == table[1][1]) {
            return true;
        }
        return false;
    }

    boolean checkDraw() {
        if (turncount == 9) {
            return true;
        }
        return false;
    }

    public Player getcurrentplayer() {
        return currentplayer;
    }

    public void switchPlayer() {
        turncount++;
        if (currentplayer == player1) {
            currentplayer = player2;
        } else {
            currentplayer = player1;
        }
    }
}
